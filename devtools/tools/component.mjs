import input from "@inquirer/input";
import fs from "fs/promises";
import path from "path";
import slugify from "slugify";
import chalk from "chalk";
import yaml from "js-yaml";

const basePath = path.resolve(process.cwd());

export default async function component(
  { componentName } = {},
  { loglevel = "" } = {}
) {
  const config = await loadConfig();

  if (!componentName) {
    componentName = await input({
      name: "componentName",
      message: "Choose a name for your new component",
    });
  }

  const componentLabel = await input({
    name: "Component Label",
    message: "Choose a storybook label for your Component",
    default: componentName,
  });

  try {
    await fs.stat(path.resolve(`${basePath}/src/components/${componentName}`));
    console.error(
      chalk.bgRed("ERROR:") +
        chalk.red(`Component ${componentName} does already exit- Abort`)
    );
    process.exit(1);
  } catch {
    console.log(
      `${chalk.blue("Start Creating component:")} ${componentName}...`
    );
  }

  const componentConfig = {
    componentName,
    componentLabel,
  };

  await fs.mkdir(path.resolve(`${basePath}/src/components/${componentName}`));

  await Promise.all(
    config.templateFiles.map(async (file) => {
      const templatePath = `${basePath}/devtools/tools/template/${file}`;
      if (loglevel === "verbose") {
        console.debug(chalk.blue("Copy Template:") + `${file}`);
      }
      const fileData = await copyTemplate(templatePath, componentConfig);
      const filePath = `${basePath}/src/components/${componentName}/${file.replace(
        "COMPONENT",
        componentName
      )}`;
      if (loglevel === "verbose") {
        console.debug(chalk.blue("Write file:") + `${filePath}`);
      }
      return fs.writeFile(path.resolve(filePath), fileData);
    })
  );
  await fs.appendFile(
    path.resolve(`${basePath}/src/components/index.ts`),
    `\nexport { default as ${componentName}} from "./${componentName}";`
  );
  await addRegisterWebcomponent(
    path.resolve(`${basePath}/src/webcomponents.tsx`),
    componentConfig
  );
  console.log(
    `${chalk.green("Finished Creating component:")} ${componentName}`
  );
  process.exit(0);
}

async function copyTemplate(filepath, { componentName, componentLabel }) {
  return fs
    .readFile(path.resolve(filepath), { encoding: "utf8" })
    .then((data) => {
      data = data.replaceAll("COMPONENTLABEL", componentLabel);
      data = data.replaceAll("COMPONENT", componentName);
      return data;
    });
}

export function getComponentConfigFromParams(param) {
  if (param.length === 2) {
    return { componentName: param[1] };
  }
  return {};
}

async function loadConfig() {
  let data = {};
  try {
    data = await fs.readFile(
      path.resolve(`${basePath}/devtools/tools/components.config.yml`)
    );
    data = yaml.load(data);
  } catch (e) {
    data = {
      templateFiles: [
        "COMPONENT.mdx",
        "COMPONENT.module.scss",
        "COMPONENT.stories.tsx",
        "COMPONENT.test.tsx",
        "COMPONENT.tsx",
        "index.ts",
      ],
    };
    await writeConfig(data);
  }
  return data;
}

async function writeConfig(data) {
  await fs.writeFile(
    path.resolve(`${basePath}/devtools/tools/components.config.yml`),
    yaml.dump(data)
  );
}
