import component, { getComponentConfigFromParams } from "./component.mjs";
import minimist from "minimist";
import select, { Separator } from "@inquirer/select";
import chalk from "chalk";

main()
  .then(() => {
    process.exit(0);
  })
  .catch((error) => {
    console.error(chalk.bgRed("[ERROR]") + " " + chalk.red(error));
    console.debug(error);
  });

async function main() {
  const argv = minimist(process.argv.slice(2));

  // console.log(argv);

  const param = argv._;

  // console.log(chalk.bgBlue('param'), param);

  if (argv.help) {
    console.log("help");
    printHelp();
    process.exit(0);
  }
  const command = await getCommand(param);
  // console.log(chalk.bgBlue('command'), command);

  switch (command) {
    case "component":
      await component(getComponentConfigFromParams(param));
      break;
    default:
      printHelp();
      process.exit(1);
  }
}

function printHelp() {
  console.log("tools <command?>");
  console.log("");
  console.log("Commands:");
  console.log("---------");
  console.log("");
  console.log("component:");
  console.log("");
  console.log(
    "tools component                              create a new component"
  );
  console.log(
    "tools component <componentName>              create a new component <componentName>"
  );
  console.log(
    "tools component <groupName> <componentName>  create a new component <groupName>/<componentName>"
  );
  console.log(
    "tools component <groupName>/<componentName>  create a new component <groupName>/<componentName>"
  );
  console.log("");
}
async function getCommand(param) {
  if (param.length === 0) {
    const command = await select({
      message: "What do you want to do",
      choices: [
        {
          name: "Add a Component",
          value: "component",
          description: "Add a new Component to the library",
        },
        {
          name: "Get Json Schema",
          value: "jsonSchema",
          description: "Generate JSON Schema from ts types",
        },
        {
          name: "Translate Terms",
          value: "translate",
          description:
            "Translate missing terms from src/lang/en.json to src/lang/de.json",
        },
        new Separator(),
        {
          name: "Help",
          value: "help",
          description: "Show me the help text",
        },
      ],
    });
    if (!command) {
      printHelp();
      process.exit(1);
    }
    return command;
  } else {
    return param[0];
  }
}
