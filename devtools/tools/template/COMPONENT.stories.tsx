import type { Meta, StoryObj } from '@storybook/react';
import { registerCOMPONENTComponent } from './COMPONENT.webcomponent';

import COMPONENT from './COMPONENT';

const meta: Meta<typeof COMPONENT> = {
  title: 'Komponenten/GROUPLABEL/COMPONENTLABEL',
  component: COMPONENT,
  argTypes: {},
};

export default meta;

type Story = StoryObj<typeof COMPONENT>;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function render(args) {
  return <COMPONENT></COMPONENT>;
}

/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/react/api/csf
 * to learn how to use render functions.
 */
export const Reactcomponent: Story = {
  render,
  args: {},
};

function renderWebcomponent(args) {
  registerCOMPONENTComponent();

  return <WEBCOMPONENT lang={args.language}></WEBCOMPONENT>;
}

export const Webcomponent: Story = {
  render: renderWebcomponent,
  args: {},
};
