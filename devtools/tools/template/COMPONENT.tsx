import { ReactElement } from 'react';
import Style from './COMPONENT.module.scss'; // eslint-disable-line @typescript-eslint/no-unused-vars
import { useIntl } from 'react-intl';

export declare interface COMPONENTProps {
  children: ReactElement[];
}

export default function COMPONENT({
  children,
}: COMPONENTProps): ReactElement<COMPONENTProps> {
  const intl = useIntl(); // eslint-disable-line @typescript-eslint/no-unused-vars
  return <>{children}</>;
}
