import type { Metadata } from "next";
import Layout from "@/components/Layout/Layout";
import "@/styles/global.scss";
import { getNavItems } from "@/lib/getNavItems";

export const metadata: Metadata = {
  title: "Codex Necropolis",
  description: "",
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const navRootItem = await getNavItems();
  return (
    <html lang="en">
      <body>
        <Layout navRoot={navRootItem}>{children}</Layout>
      </body>
    </html>
  );
}
