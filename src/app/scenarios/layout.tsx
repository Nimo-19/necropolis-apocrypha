import type { Metadata } from "next";
import Article from "@/components/Article/Article";
import "@/styles/global.scss";

export const metadata: Metadata = {
  title: "Codex Necropolis",
  description: "",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return <Article>{children}</Article>;
}
