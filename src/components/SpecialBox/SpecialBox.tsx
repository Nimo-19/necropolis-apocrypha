import { ReactNode, useId } from "react";
import Style from "./SpecialBox.module.scss";

export declare interface SpecialBoxProps {
  title: string;
  children: ReactNode;
}

export default function SpecialBox({ title, children }: SpecialBoxProps) {
  return (
    <aside className={Style.wrapper}>
      <h3 className={Style.title}>{title}</h3>
      <div className={[Style.content, "content"].join(" ")}>{children}</div>
    </aside>
  );
}
