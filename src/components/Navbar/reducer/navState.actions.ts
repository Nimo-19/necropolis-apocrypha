import {
  NAVIGATE_CHILD,
  NAVIGATE_PAGE,
  NAVIGATE_PARENT,
  NavStateActionType,
} from "./navState.constants";

export declare interface NavStateAction {
  type: NavStateActionType;
  key?: string;
  path?: string[];
}

export function navigateChild(key: string): NavStateAction {
  return {
    type: NAVIGATE_CHILD,
    key,
  };
}

export function navigateParent(): NavStateAction {
  return {
    type: NAVIGATE_PARENT,
  };
}

export function navigatePage(path: string[]): NavStateAction {
  return {
    type: NAVIGATE_PAGE,
    path,
  };
}
