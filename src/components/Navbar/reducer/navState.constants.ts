export type NavStateActionType =
  | "NAVIGATE_CHILD"
  | "NAVIGATE_PARENT"
  | "NAVIGATE_PAGE";

export const NAVIGATE_PAGE: NavStateActionType = "NAVIGATE_PAGE";
export const NAVIGATE_CHILD: NavStateActionType = "NAVIGATE_CHILD";
export const NAVIGATE_PARENT: NavStateActionType = "NAVIGATE_PARENT";
