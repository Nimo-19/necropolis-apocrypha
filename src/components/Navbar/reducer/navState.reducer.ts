import { NavItem } from "../models/NavItem";
import { NavStateAction } from "./navState.actions";

export declare interface NavState {
  currentPath: string[];
}

export default function navStateReducer(
  state: NavState,
  action: NavStateAction
): NavState {
  switch (action.type) {
    case "NAVIGATE_CHILD":
      return {
        ...state,
        currentPath: state.currentPath.concat([action.key as string]),
      };
    case "NAVIGATE_PARENT":
      return {
        ...state,
        currentPath: state.currentPath.slice(0, -1),
      };
    case "NAVIGATE_PAGE":
      return {
        ...state,
        currentPath: [...(action.path as string[])],
      };
    default:
      return state;
  }
}

export function createDefaultState({
  rootItem,
  initialPath,
}: {
  rootItem: NavItem;
  initialPath: string;
}): NavState {
  if (!initialPath.startsWith("/")) {
    return { currentPath: [] };
  }
  const path = initialPath.split("/").splice(1);

  let tmpItem = rootItem;
  let currentPath: string[] = [];

  for (const key in path) {
    if (tmpItem.children !== undefined && key in tmpItem.children) {
      if (tmpItem.children[key].children !== undefined) {
        tmpItem = tmpItem.children[key];
        currentPath = currentPath.concat([key]);
      } else {
        break;
      }
    } else {
      break;
    }
  }

  return {
    currentPath,
  };
}
