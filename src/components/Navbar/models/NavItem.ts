export declare interface NavItem {
  title: string;
  numeral: string;
  target: string;
  children?: Record<string, NavItem>;
}
