"use client";
import {
  MouseEvent,
  forwardRef,
  useCallback,
  useImperativeHandle,
  useMemo,
  useReducer,
  useRef,
} from "react";
import Style from "./NavBar.module.scss";
import { NavItem } from "./models/NavItem";
import navStateReducer, {
  createDefaultState,
} from "./reducer/navState.reducer";
import Link from "next/link";
import NavLink from "./components/NavLink";
import { navigateChild, navigateParent } from "./reducer/navState.actions";
import useFocusTrap, { FOCUSABLE_ELEMENTS } from "@/lib/hooks/useFocusTrap";
import Image from "next/image";

export declare interface NavBarProps {
  initialPath?: string;
  rootItem: NavItem;
  hasActiveFocusTrap?: boolean;
}

const NavBar = forwardRef(
  (
    { rootItem, initialPath = "/", hasActiveFocusTrap = false }: NavBarProps,
    ref
  ) => {
    const [state, dispatch] = useReducer(
      navStateReducer,
      { rootItem, initialPath },
      createDefaultState
    );
    const { currentPath } = state;

    const [currentItem, parentItem] = useMemo(
      () => pathToItem(rootItem, currentPath),
      [currentPath, rootItem]
    );

    function handleNavigate(e: MouseEvent, key: string) {
      e.preventDefault();
      dispatch(navigateChild(key));
    }
    function handleNavigateBack(e: MouseEvent) {
      e.preventDefault();
      dispatch(navigateParent());
    }

    const handleKeyboard = useCallback(
      (
        evt: KeyboardEvent,
        focusableElements: HTMLElement[],
        currentFocusIdx: number
      ) => {
        if (evt.key === "ArrowDown") {
          evt.preventDefault();
          focusableElements[
            (currentFocusIdx + 1) % focusableElements.length
          ].focus();
        } else if (evt.key === "ArrowUp") {
          evt.preventDefault();
          focusableElements[
            currentFocusIdx === 0
              ? focusableElements.length - 1
              : currentFocusIdx - 1
          ].focus();
        } else if (evt.key === "ArrowLeft") {
          evt.preventDefault();
          if (parentItem !== undefined) {
            focusableElements[2].focus(); // tmp. please select title and back in a better way;
          } else {
            focusableElements[0].focus();
          }
        } else if (evt.key === "ArrowRight") {
          focusableElements[currentFocusIdx].click();
        }
      },
      [parentItem]
    );

    const navBarRef = useFocusTrap({
      isActiveTrap: hasActiveFocusTrap,
      additionalHandler: handleKeyboard,
      additionalDeps: [currentItem],
    });

    const focusElm = useRef(null);

    const focusOnOpen = useCallback(() => {
      console.log("focus?!");
      console.log("focusElm", focusElm);
      if (focusElm.current) {
        const current = focusElm.current as HTMLElement;
        current.focus({ focusVisible: true });
        console.log("focusElm", focusElm);
        console.log("active", document.activeElement);
      }
    }, [focusElm]);

    useImperativeHandle(ref, () => {
      return {
        focus: focusOnOpen,
      };
    });

    return (
      <div className={Style.navBar} ref={navBarRef}>
        <li className={Style.title}>
          <Link className="interactable" href="/">
            {rootItem.title}
          </Link>
        </li>
        {currentItem !== rootItem && (
          <>
            <li className={Style.chapter}>
              <Link className="interactable" href={currentItem.target}>
                Chapter {currentItem.numeral}: {currentItem.title}
              </Link>
            </li>
            <li>
              <a
                className={Style.backLink}
                href={parentItem.target}
                aria-label="go up one navigation level"
                onClick={handleNavigateBack}
              ></a>
            </li>
          </>
        )}
        {currentItem.children !== undefined &&
          Object.entries(currentItem.children).map(([key, child], idx) => {
            if (child.children !== undefined) {
              return (
                <li key={key}>
                  <a
                    href={child.target}
                    className={Style.navLink}
                    onClick={(e) => handleNavigate(e, key)}
                    ref={idx === 0 ? focusElm : undefined}
                  >
                    <NavLink title={child.title} numeral={child.numeral} />
                  </a>
                </li>
              );
            }
            return (
              <li key={key}>
                <Link
                  href={child.target}
                  className={Style.navLink}
                  ref={idx === 0 ? focusElm : undefined}
                >
                  <NavLink title={child.title} numeral={child.numeral} />
                </Link>
              </li>
            );
          })}
        <div className={Style.decoration}>
          <Image
            src="/assets/images/skull-inkwell-ghost-quill.webp"
            width={256}
            height={341}
            alt="a skull shaped inkwell with a ghostly quill"
          />
        </div>
      </div>
    );
  }
);

export default NavBar;

export function pathToItem(root: NavItem, path: string[]): NavItem[] {
  if (path.length === 0) {
    return [root];
  }
  return path.reduce(
    ([currentItem, ...parentItems]: NavItem[], key: string) => {
      if (currentItem.children !== undefined && key in currentItem.children) {
        return [currentItem.children[key]].concat([currentItem], parentItems);
      } else {
        console.error(
          `Error: ${key} not found in ${currentItem.title}. Skipping this key`,
          currentItem
        );
        return [currentItem, ...parentItems];
      }
    },
    [root]
  );
}
