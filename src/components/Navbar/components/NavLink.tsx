import Style from "../NavBar.module.scss";

export declare interface NavLinkProps {
  numeral: string;
  title: string;
}
export default function NavLink({ title, numeral }: NavLinkProps) {
  return (
    <>
      <div className={[Style.navLinkTitle, Style.interactable].join(" ")}>
        {title}
      </div>
      <div className={Style.navLinkDots}>
        ..............................................................................
      </div>
      <div className={[Style.navLinkNumeral].join(" ")}>{numeral}</div>
    </>
  );
}
