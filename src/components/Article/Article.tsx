import { ReactNode } from "react";
import Style from "./Article.module.scss";

export declare interface ArticleProps {
  children: ReactNode;
}

export default function Article({ children }: ArticleProps) {
  return <article className={Style.content}>{children}</article>;
}
