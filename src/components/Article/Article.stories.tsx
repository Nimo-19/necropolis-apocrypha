import type { Meta, StoryObj } from "@storybook/react";

import SpecialBox from "../SpecialBox/SpecialBox";
import Article from "./Article";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: "Components/Article",
  component: Article,
  parameters: {},
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ["autodocs"],
  // More on argTypes: https://storybook.js.org/docs/api/argtypes
  argTypes: {},
} satisfies Meta<typeof Article>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Primary: Story = {
  args: {
    children: (
      <>
        <h1>Headline 1</h1>
        <h2>Headline 2</h2>
        <h3>Headline 3</h3>
        <h4>Headline 4</h4>
        <h5>Headline 5</h5>
        <p>
          Paragraph Lorem ipsum dolor sit amet, officia excepteur ex fugiat
          reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex
          esse exercitation amet. Nisi anim cupidatat excepteur officia.
          Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate
          voluptate dolor minim nulla est proident. Nostrud officia pariatur ut
          officia.
        </p>
        <p>
          Lorem ipsum <a href="#">Link</a> dolor sit amet,
          <strong>Strong</strong> qui minim <i>Italique</i> labore adipisicing
          minim sint cillum sint consectetur cupidatat.
        </p>
        <p>
          Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo
          officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa
          proident adipisicing id nulla nisi laboris ex in Lorem sunt duis
          officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis
          sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea
          consectetur et est culpa et culpa duis.
        </p>
        <SpecialBox title="Special Box">
          <p>
            Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint
            cillum sint consectetur cupidatat.
          </p>
          <p>
            Qui minim labore adipisicing minim sint cillum sint consectetur
            cupidatat.
          </p>
          <h4>Headline 4</h4>
          <p>
            Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint
            cillum sint consectetur cupidatat.
          </p>
          <p>
            Qui minim labore adipisicing minim sint cillum sint consectetur
            cupidatat.
          </p>
        </SpecialBox>
      </>
    ),
  },
};
