import { ReactNode } from "react";
import Style from "./Layout.module.scss";
import NavBar from "../Navbar/NavBar";
import { NavItem } from "../Navbar/models/NavItem";
import Menu from "../Menu/Menu";

export declare interface LayoutProps {
  navRoot?: NavItem;
  children: ReactNode;
}

export default function Layout({ children, navRoot }: LayoutProps) {
  return (
    <div className={Style.wrapper}>
      {navRoot && <Menu rootItem={navRoot} />}
      <main className={[Style.content, "rough-box"].join(" ")}>{children}</main>
    </div>
  );
}
