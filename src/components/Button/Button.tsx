import { MouseEventHandler, ReactNode, useId } from "react";
import Style from "./Button.module.scss";

export declare interface ButtonProps {
  children: ReactNode;
  ariaLabel: string;
  onClick: MouseEventHandler;
}

export default function Button({ children, ariaLabel, onClick }: ButtonProps) {
  return (
    <button
      aria-label={ariaLabel}
      className={["interactable"].join(" ")}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
