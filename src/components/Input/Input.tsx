import { ReactNode, useId } from "react";
import Style from "./Input.module.scss";

export declare interface InputProps {
  label: string;
}

export default function Input({ label }: InputProps) {
  const id = useId();
  return (
    <div className={Style.formField}>
      <input id={id} type="text" className={Style.input} />
      <label htmlFor={id} className={Style.label}>
        {label}
      </label>
    </div>
  );
}
