"use client";

import { KeyboardEvent, useRef, useState } from "react";
import Style from "./Menu.module.scss";
import { NavItem } from "../Navbar/models/NavItem";
import NavBar from "../Navbar/NavBar";
import useMediaQuery from "@/lib/hooks/useMediaQuery";

export declare interface MenuProps {
  rootItem: NavItem;
}

export type MenuAnimationState = "OPEN" | "CLOSED" | "OPENING" | "CLOSING";

export default function Menu({ rootItem }: MenuProps) {
  const showDesktopMenu = useMediaQuery("(min-width: 1200px)");
  const [open, setOpen] = useState<MenuAnimationState>("CLOSED");
  const navBarChildRef = useRef(null);
  const openBtnRef = useRef(null);

  function handleStateChange() {
    switch (open) {
      case "OPEN":
        setOpen("CLOSING");
        break;
      case "CLOSED":
        setOpen("OPENING");
        break;
      case "OPENING":
        setOpen("OPEN");
        break;
      case "CLOSING":
        setOpen("CLOSED");
        break;
    }
  }

  function handleAnimationFinished() {
    if (open === "OPENING" && navBarChildRef.current) {
      navBarChildRef.current.focus();
    }
    if (open === "CLOSING" && openBtnRef.current) {
      openBtnRef.current.focus();
    }
    handleStateChange();
  }

  function keyBoardHandler(e: KeyboardEvent) {
    if (open && e.key === "Escape") {
      e.preventDefault();
      setOpen("CLOSING");
    }
  }

  return (
    <nav
      className={[Style.menu, showDesktopMenu ? "" : Style[open]].join(" ")}
      onClick={() => handleStateChange()}
    >
      {!showDesktopMenu && (
        <button
          className={Style.openMenu}
          onClick={handleStateChange}
          ref={openBtnRef}
          aria-label="Open Menu"
          title="Open Menu"
        ></button>
      )}

      <div
        className={[Style.box, "rough-box"].join(" ")}
        onKeyUp={keyBoardHandler}
        onAnimationEnd={handleAnimationFinished}
        onClick={(evt) => evt.stopPropagation()}
      >
        <button
          className={Style.closeMenu}
          onClick={handleStateChange}
          aria-label="Close Menu"
          title="Close Menu"
        >
          Close
        </button>
        <div className={Style.closeMenuWrapper}></div>
        <NavBar
          rootItem={rootItem}
          hasActiveFocusTrap={!showDesktopMenu && open === "OPEN"}
          ref={navBarChildRef}
        />
      </div>
    </nav>
  );
}
