import type { Meta, StoryObj } from "@storybook/react";

import Menu from "./Menu";
import { KeyboardEvent } from "react";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: "Components/Menu",
  component: Menu,
  parameters: {},
  // This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ["autodocs"],
  // More on argTypes: https://storybook.js.org/docs/api/argtypes
  argTypes: {},
  decorators: [
    (Story) => {
      function mockMissingClickOnEnter(e: KeyboardEvent) {
        if (e.key === "Enter") {
          console.log("mock click on: ", e.target);
          (e.target as HTMLElement).click();
        }
      }
      return (
        <div onKeyUp={mockMissingClickOnEnter}>
          <Story />
        </div>
      );
    },
  ],
} satisfies Meta<typeof Menu>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Default: Story = {
  args: {
    rootItem: {
      title: "Necropolis Apocrypha",
      numeral: "0",
      target: "/",
      children: {
        scenarios: {
          title: "Scenarios",
          numeral: "I",
          target: "/scenarios",
          children: {
            "locked-tombs": {
              title: "Locked Tombs",
              numeral: "i",
              target: "/scenarios",
            },
            "angry-jinn": {
              title: "Angry Jinn",
              numeral: "ii",
              target: "/scenarios",
            },
          },
        },
        cards: {
          title: "Cards",
          numeral: "I",
          target: "/cards",
        },
      },
    },
  },
};
