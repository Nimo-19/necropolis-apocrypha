import { NavItem } from "@/components/Navbar/models/NavItem";
import { readFile, readdir } from "fs/promises";
import path from "path";
import YAML from "yaml";

export declare interface ProtoItem {
  title: string;
  navPath: string[];
}

export async function getNavItems() {
  const dir = await readdir(path.join(process.cwd(), "src/app"), {
    recursive: true,
  });
  const filtered = dir
    .flat()
    .filter((name) => name.endsWith("page.yaml"))
    .map((name: string) => name.split("/").slice(0, -1));
  //.map((name: string) => name.replaceAll(/\/*page\.(tsx|mdx|jsx)/g, ""));
  const data = await getItemMetaData(filtered);
  const items = getItemsFromProtoData(data);
  // console.log(JSON.stringify(items, null, "  "));
  return items;
}

async function getItemMetaData(paths: string[][]): Promise<ProtoItem[]> {
  return Promise.all(
    paths.map(async (dataPath: string[]) => {
      return readFile(
        path.join(process.cwd(), "src/app", ...dataPath, "page.yaml"),
        {
          encoding: "utf8",
        }
      ).then((content: string) => {
        const data: Partial<ProtoItem> = YAML.parse(content);
        return {
          navPath: dataPath,
          ...data,
        } as ProtoItem;
      });
    })
  );
}

export function getItemsFromProtoData(
  items: ProtoItem[],
  currentPath: string = "/",
  depths = 0,
  index = 0
): NavItem | undefined {
  const currentItem = items.find(({ navPath }) => navPath.length === 0);
  if (items.length === 0 || currentItem === undefined) {
    return;
  }
  if (items.length > 1) {
    const subPaths = items.filter(({ navPath }) => navPath.length === 1);

    const { navPath, ...data } = currentItem;
    return {
      target: currentPath,
      numeral: getNumeral(depths, index),
      ...data,
      children: Object.fromEntries(
        subPaths.map((childItem: ProtoItem, idx) => [
          childItem.navPath[0],
          getItemsFromProtoData(
            items
              .filter(({ navPath }) => navPath[0] === childItem.navPath[0])
              .map((item) => ({ ...item, navPath: item.navPath.slice(1) })),
            currentPath + childItem.navPath.join("/") + "/",
            depths + 1,
            idx + 1
          ),
        ])
      ) as Record<string, NavItem>,
    };
  } else {
    const { navPath, ...data } = currentItem;
    return {
      target: currentPath,
      numeral: getNumeral(depths, index),
      ...data,
    };
  }
}

function getNumeral(depths: number, index: number): string {
  switch (depths) {
    case 2:
      return convertToRoman(index).toLowerCase();
    case 3:
      return String.fromCharCode(index + 96);
    default:
      return convertToRoman(index);
  }
}

function convertToRoman(num: number): string {
  const roman: Record<string, number> = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1,
  };
  let str = "";

  for (const key of Object.keys(roman)) {
    const q = Math.floor(num / roman[key]);
    num -= q * roman[key];
    str += key.repeat(q);
  }

  return str;
}
