import { expect, describe, it } from "vitest";
import { ProtoItem, getItemsFromProtoData } from "./getNavItems";
import { NavItem } from "@/components/Navbar/models/NavItem";

describe("Parse NavItems from Proto Data", () => {
  it("creates only a root object", () => {
    const protoData = [{ navPath: [], title: "root" }];

    const navItems = getItemsFromProtoData(protoData);

    expect(navItems).toEqual({ title: "root", target: "/", numeral: "" });
  });
  it("creates complex tree", () => {
    const protoData: ProtoItem[] = pathsToMockProtoData([
      "",
      "test",
      "test/foo",
      "test/bar",
      "test2",
      "test2/foo",
      "test2/bar",
      "test2/testing",
      "test2/testing/foo",
      "test2/testing/bar",
    ]);

    const navItems = getItemsFromProtoData(protoData);

    expect(navItems).toEqual({
      title: "",
      target: "/",
      numeral: "",
      children: {
        test: {
          title: "test",
          target: "/test/",
          numeral: "I",
          children: {
            foo: {
              title: "foo",
              target: "/test/foo/",
              numeral: "i",
            },
            bar: {
              title: "bar",
              target: "/test/bar/",
              numeral: "ii",
            },
          },
        },
        test2: {
          title: "test2",
          target: "/test2/",
          numeral: "II",
          children: {
            foo: {
              title: "foo",
              target: "/test2/foo/",
              numeral: "i",
            },
            bar: {
              title: "bar",
              target: "/test2/bar/",
              numeral: "ii",
            },
            testing: {
              title: "testing",
              target: "/test2/testing/",
              numeral: "iii",
              children: {
                foo: {
                  title: "foo",
                  target: "/test2/testing/foo/",
                  numeral: "a",
                },
                bar: {
                  title: "bar",
                  target: "/test2/testing/bar/",
                  numeral: "b",
                },
              },
            },
          },
        },
      },
    });
  });
});

function pathsToMockProtoData(paths: string[]): ProtoItem[] {
  return paths.map((currentPath) => {
    return {
      navPath: currentPath.split("/").filter((str) => str !== ""),
      title: currentPath.split("/").at(-1) as string,
    };
  });
}
