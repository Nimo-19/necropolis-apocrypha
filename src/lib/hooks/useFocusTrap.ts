import { useEffect, useRef } from "react";

export declare interface FocusTrapOptions {
  additionalHandler?: (
    evt: KeyboardEvent,
    focusableElements: HTMLElement[],
    currentFocusIdx: number,
    isActiveTrap?: boolean
  ) => void;
  isActiveTrap?: boolean;
  additionalDeps?: any[];
}

export const FOCUSABLE_ELEMENTS = [
  "a[href]",
  "button",
  "input",
  "textarea",
  "select",
  "details",
  '[tabcurrentFocusIdx]:not([tabindex="-1"])',
];

export default function useFocusTrap({
  isActiveTrap = true,
  additionalHandler,
  additionalDeps = [],
}: FocusTrapOptions) {
  const ref = useRef(null);

  useEffect(() => {
    if (ref.current) {
      const current: HTMLElement = ref.current as HTMLElement;
      const focusableElements = Array.from(
        current.querySelectorAll(FOCUSABLE_ELEMENTS.join(", "))
      ).filter(
        (el) => !el.hasAttribute("disabled") && !el.getAttribute("aria-hidden")
      ) as HTMLElement[];
      const keyboardHandler = (evt: KeyboardEvent) => {
        const currentFocus = document.activeElement as HTMLElement;
        const currentFocusIdx = focusableElements.indexOf(currentFocus);
        if (currentFocusIdx < 0) {
          return;
        }
        if (
          isActiveTrap &&
          evt.shiftKey &&
          evt.key === "Tab" &&
          currentFocusIdx === 0
        ) {
          evt.preventDefault();
          (focusableElements.at(-1) as HTMLElement).focus();
        } else if (
          isActiveTrap &&
          !evt.shiftKey &&
          evt.key === "Tab" &&
          currentFocusIdx === focusableElements.length - 1
        ) {
          evt.preventDefault();
          focusableElements[0].focus();
        }
        if (additionalHandler !== undefined) {
          additionalHandler(
            evt,
            focusableElements,
            currentFocusIdx,
            isActiveTrap
          );
        }
        return true;
      };

      current.addEventListener("keydown", keyboardHandler, {
        capture: true,
        passive: false,
      });

      return () => current.removeEventListener("keydown", keyboardHandler);
    }
  }, [ref, isActiveTrap, additionalHandler, ...additionalDeps]);

  return ref;
}
