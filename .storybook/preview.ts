import type { Preview } from "@storybook/react";

import "@/styles/global.scss";

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    backgrounds: {
      default: "dark",
      values: [
        {
          name: "dark",
          value: "#1b1e1d",
        },
        {
          name: "light",
          value: "#b4b6ac",
        },
        {
          name: "black",
          value: "#030500",
        },
        {
          name: "white",
          value: "#fafbf9",
        },
      ],
    },
  },
};

export default preview;
